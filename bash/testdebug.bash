#prerequisites:
# * Make sure your snippet in the dynapp has code.interact set. See https://gitlab.com/slam-ug/sl1-snippets/blob/master/python/debug/debug_breakpoint.py for an example
# * Make sure the dynapp is aligned to a device
# * Make sure you run this on the collector actually running the script

#Steps
# * SSH to a collector or AIO system
# * run the following entry, 
#   * replace <did> with the actual device id
#   * replace <aid> with the application id

sudo -H -u s-em7-core bash -c "SILO_DEBUG=0 /opt/em7/backend/dynamic_single.py <did> <aid>"