# Bash scripts for the SL1 Platform

If you have useful scripts to run on the sl1 devices, here is the place to store them.