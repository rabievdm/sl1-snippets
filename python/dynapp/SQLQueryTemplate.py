"""This snippet template can be used for Snippet Performance, Snippet Configuration, or Snippet Journal applications.
This is especially helpful for SaaS ScienceLogic customers. You can execute database queries from a device aligned to the SaaS collector
that ScienceLogic provisions. However, it can be adapted for any MySQL database you'd like.
Potential use cases:
    Dynamic app to alert when you see SIGTERMs on collectors (suggested to limit to certain number)
    Trend number of events/tickets for a given organization
    Alert when you see formula errors in the SL1 System Log table
"""
import MySQLdb
import silo_common.snippets as em7_snippets

SNIPPET_NAME = "APP NAME | VER 0.5"  # TODO: Rename
RESULTS = {}


# Functions:
def var_dump(val):
    import pprint
    pp = pprint.PrettyPrinter(indent=0)
    pp.pprint(val)


# simple logger
def logger_debug(sev_level=6, log_message=None, log_var=None):
    log_sev_types = {0: 'EMERGENCY', 1: 'ALERT', 2: 'CRITICAL', 3: 'ERROR', 4: 'WARNING', 5: 'NOTICE', 6: 'INFORMATION',
                     7: 'DEBUG'}
    if log_message is not None and log_var is not None:
        self.logger.ui_debug("[%s] %s %s" % (
            log_sev_types[sev_level], str(log_message), str(log_var)))
    elif log_message is not None:
        self.logger.ui_debug("[%s] %s" %
                             (log_sev_types[sev_level], str(log_message)))


def mysql_connect():
    """Get a MySQL connection object
    """
    try:
        conn = MySQLdb.connect(user=self.cred_details['cred_user'], passwd=self.cred_details['cred_pwd'],
                               host=self.cred_details['cred_host'], port=int(self.cred_details['cred_port']))
        if conn and conn is not None:
            try:
                db_dict = {}
                db_dict['conn'] = conn
                db_dict['conn'].set_character_set('utf8')
                db_dict['cur'] = conn.cursor(
                    cursorclass=MySQLdb.cursors.DictCursor)
                db_dict['cur'].execute('SET NAMES utf8;')
                db_dict['cur'].execute('SET CHARACTER SET utf8;')
                db_dict['cur'].execute('SET character_set_connection = utf8;')
                return db_dict

            except MySQLdb.Error as e:
                logger_debug(2, 'MySQL Problem:', str(e))
            except Exception as e:
                logger_debug(1, 'Silo Config File Exception: ', str(e))
            except:
                logger_debug(2, 'MySQL Unknown Error:', '')
        else:
            logger_debug(2, 'MySQL Connection Error', '')
    except Exception as e:
        logger_debug(1, 'Silo Config File Exception: ', str(e))
    except:
        logger_debug(2, 'MySQL Unknown Error:', '')
    return None


def mysql_close(db_dict):
    if db_dict is not None \
            and isinstance(db_dict, dict) \
            and db_dict['conn'] is not None \
            and db_dict['cur'] is not None:
        db_dict['cur'].close()
        db_dict['conn'].close()
        logger_debug(7, 'Closing MySQL Connection', '')


def fetch_noncat_events(db_dict):  # TODO: Replace with your preferred function
    """Accepts a MySQL connection object to execute your query against
    """
    event_list = []
    if isinstance(db_dict, dict):
        query = "SELECT * FROM `master`.`debug`"
        db_dict['cur'].execute(query)
        for row in db_dict['cur'].fetchall():
            if isinstance(row, dict):
                event_list.append(str(row['id']))
    return event_list


# Main:
# var_dump(self.cred_details)

if self.cred_details['cred_type'] == 5:  # SOAP/XML
    try:
        # If executing against SciLo database, set cred port to 7706 and input user/pass with hostname/IP hardcoded
        DB_Dict = mysql_connect()

        if isinstance(DB_Dict, dict):
            # TODO: Execute specific queries. Create them as functions above and execute them here.
            # EXAMPLE: EV_List = fetch_noncat_events(DB_Dict)
            INSERT_VAR_HERE = fetch_noncat_events(DB_Dict)
            var_dump(INSERT_VAR_HERE)  # Good to see what we're working with

            # Don't forget this, you want to make sure the connection is closed.
            mysql_close(DB_Dict)

            for group, oid_group in self.oids.iteritems():
                for obj_id, oid_detail in oid_group.iteritems():
                    oid = oid_detail['oid']
                    if oid in result_stats:
                        RESULTS[oid] = [((0, result_stats[oid]))]

        # return results to em7
        result_handler.update(RESULTS)

    except Exception as e:
        COLLECTION_PROBLEM = True
        PROBLEM_STR = str(e)
        logger_debug(2, PROBLEM_STR)
    except:
        COLLECTION_PROBLEM = True
        PROBLEM_STR = "%s: Unknown Exception Occurred." % (SNIPPET_NAME)
        logger_debug(2, PROBLEM_STR)
else:
    COLLECTION_PROBLEM = True
    PROBLEM_STR = "%s: Wrong Credential Type Aligned to This Dynamic Application (Required XML/SOAP)" % (
        SNIPPET_NAME)
    logger_debug(3, PROBLEM_STR)
