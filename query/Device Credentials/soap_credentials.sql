select sc.cred_id, sc.cred_name, sc.cred_user
    #, sc.cred_pwd # is commented out because it''s value is hashed
    , sc.cred_host, sc.cred_port
    , dct.name as credential_type_name
    , sc.cred_timeout
    , if(sc.all_orgs=1,'all organizations', maco.orgs )
    , mscc.cred_id            
    , mscc.curl_url           
    , mscc.curl_timeout       
    , mscc.curl_proxy_ip      
    , mscc.curl_proxy_port    
    , mscc.curl_proxy_acct    
    # , mscc.curl_proxy_passwd  # hidden because it is hidden
    , mscc.curl_ssl_mode      
    , mscc.curl_encoding      
    , mscc.curl_post_or_get   
    , mscc.curl_http_version  
    # , mscc.curl_request_pwd   # hidden because it is hashed
    , mscc.curl_request_sub_1 
    , mscc.curl_request_sub_2 
    , mscc.curl_request_sub_3 
    , mscc.curl_request_sub_4 
    , mscc.curl_header
    , mscc.curl_options
from master.system_credentials as sc
inner join master.definitions_credential_types dct on sc.cred_type=dct.cred_type
inner join master.system_credentials_curl as mscc on mscc.cred_id = sc.cred_id
left join (
    select co.cred_id, group_concat(o.company separator ',') as orgs 
    from master_access.credentials_organizations as co 
    inner join master_biz.organizations as o on co.roa_id = o.roa_id
    group by cred_id
) as maco on sc.cred_id = maco.cred_id