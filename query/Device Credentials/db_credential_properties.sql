select cred_id, db_type, db_name, db_sid
, db_connect # 0 = N/A or SID, 1 = SERVICE, 2=SERVER
from master.system_credentials_db
order by db_name;