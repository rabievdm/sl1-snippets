select cred_id
, ldap_type #1 = Active Directory, 0 = ldap
,ldap_domain, ldap_root_dn, ldap_user_dn, ldap_secure
, ldap_scope # 0= subtree, 1=one-level
from master.system_credentials_ldap
order by ldap_domain;