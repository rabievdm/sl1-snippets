SELECT dev.id,
         dev.device,
         interface.ifDescr,
         interface.alias,
         ip.ip,
         ip.netmask
FROM master_dev.legend_device dev
JOIN master_dev.device_ip_addr ip
    ON dev.id = ip.did
JOIN master_dev.device_interfaces interface
    ON ip.if_id = interface.if_id
WHERE dev.roa_id = <ORGANIZATION_ID>