# WARNING: This will change EVERY expiration time for all event policies of a specific severity!!
# DO NOT RUN THIS unless you know what you are doing! This is not supported by ScienceLogic.
# I highly recommend the following query to get your previous expiry times in case you need to roll back:
# SELECT * FROM master.policies_events WHERE eseverity = <SEVERITY>

UPDATE master.policies_events SET expiry_time = <MINUTES>
WHERE eseverity = <SEVERITY>;