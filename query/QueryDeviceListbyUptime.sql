#get Devices sorted by uptime
	
	SELECT device, DATE_SUB(snmp.last_poll, INTERVAL snmp.sysuptime/100 SECOND) AS LastReboot
	FROM master_biz.organizations org
	JOIN master_dev.legend_device dev ON (org.roa_id = dev.roa_id)
	LEFT JOIN master_dev.device_snmp_data snmp ON dev.id=snmp.did
	where sysuptime > 0
    order by lastreboot asc