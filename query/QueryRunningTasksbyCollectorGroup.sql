# query to find outstanding task for a specified collector Group (by collector group name)
# Dynamic App Status from (master_logs.spool_process.state)
# 0	complete
# 1	scheduled
# 2	launched
# 3	running
# 4	failed
# 5	canceled


Select count(*), master_logs.spool_process.state, master.system_collector_groups.cug_name
	from master_logs.spool_process
	INNER JOIN master.system_collector_groups_to_collectors
	ON master_logs.spool_process.mid=master.system_collector_groups_to_collectors.pid
	INNER JOIN master.system_collector_groups
	ON master.system_collector_groups_to_collectors.cug_id = master.system_collector_groups.cug_id
	where master.system_collector_groups.cug_name = [your collector Name] and state not in (0,4,5)
	group by master_logs.spool_process.state
	order by count(*)
