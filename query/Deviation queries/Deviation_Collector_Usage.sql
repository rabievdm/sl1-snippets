select dhs.obj_id, da.name as daname, dao.name as daoname, dhs.did as deviceid, dhs.*
from master.dynamic_app as da
inner join master.dynamic_app_objects as dao on da.app_guid = dao.app_guid
inner join collector_state.da_hourly_stats as dhs on dao.obj_id = dhs.obj_id
limit 10