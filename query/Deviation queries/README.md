# ScienceLogic Deviation workflow

## Summary

This folder contains queries to find out whether your deviation is working as expected. It consists of 2 parts: queries on the central database and queries on the collector.
The deviation process consists of 2 separate processes. It is invoked on the DB, but all collectors calculate the deviation for the individual devices and collection objects themselves.
They also store the results and (at this point) I don't think these results are pushed back to the CDB.

## Central Database queries

### Summary queries

* "Deviation_CDB_ProcessRuns.sql" contains a query to see how long the deviation process on the CDB is taking before completion OR SIGTERM'ing. You can use this info to tune the process 167 via System -> Settings -> Processes.
* The same applies for "Deviation_Collector_ProcessRuns.sql", which can also be run on the CDB. This will show you how long each process runs on an individual collector (using \<collector_id\> as a replacement parameter for the collector ID found in System -> Settings -> Appliances).

### Status Queries

Once you know how your processes are doing, you can investigate how current your deviation data might be.

* "Deviation_CDB_Summary.sql" shows you what the CDB 'thinks' is the current status for each device, each dynamic application, every collection object AND every instance for this object.

## Collector queries

### Deviation process count

All processes sent to collectors can be queried via file "Deviation_Collector_Processes.sql"

### Deviation data

On every collector, you can query whether the deviation data is actually present in the system using the query in "Deviation_Collector_Usage.sql" file.
