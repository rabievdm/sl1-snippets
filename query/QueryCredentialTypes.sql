SELECT * FROM master.definitions_credential_types; 

-- cred_type 	 name 	 ui_category 	 schema 
-- 1	SNMP	snmp	
-- 2	Database	db	
-- 3	SOAP/XML Host	soap	
-- 4	LDAP/AD	ldap	
-- 5	Basic/Snippet	basic	
-- 6	SSH/Key	ssh	
-- 7	PowerShell	powershell