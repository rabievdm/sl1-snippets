#query to find outstanding task for a specified collector (by collector ID)

# Dynamic App Status from (master_logs.spool_process.state)
# 0	complete
# 1	scheduled
# 2	launched
# 3	running
# 4	failed
# 5	

select count(*), state
	from master_logs.spool_process
	where mid = 11 and state not in (0,4,5)
	group by state
    order by count(*)