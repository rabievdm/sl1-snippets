SELECT MDLD.id AS did,
         MDLD.device AS device_name,
         MDLD.ip AS ip_address,
         MDDI.if_id AS if_id,
         MDDI.ifPhysAddress AS mac_address,
         MDDI.ifDescr AS if_descrption,
         MDDI.alias,
         MEEM.modifier_id AS modifier,
         MDLD.roa_id,
         MDDI.alerts,
         MDDI.state
FROM master_events.entity_modifier AS MEEM
JOIN master_dev.legend_device AS MDLD
    ON MEEM.xid = MDLD.id
JOIN master_dev.device_interfaces AS MDDI
    ON MEEM.yid = MDDI.if_id
WHERE MEEM.ytype=7
        AND MEEM.xtype=1
        AND MEEM.modifier_id != 4
        AND MDLD.roa_id=177
        AND MDDI.alerts=1
        AND MDDI.state=1
ORDER BY  device_name, if_id